window.cs = new content_script();
var ext = null;
cs.on(function(info){
	if(info.type==="copy" && ext){
		let xpath=cs.GetXPath(ext);
		cs.Copy(xpath);
	}else if(info.type==="copySelect" && ext){
		let xpath=cs.GetSelector(ext);
		cs.Copy(xpath);
	}
});
$(() => {
	var f=e => {
		if (ext) {
			ext.style.border = "";
			ext = null;
		}
		ext = e.target;
		ext.style.boxSizing="border-box";
		ext.style.border = "1px solid red";
	};
setInterval(()=>{
	$("*").hover(f, e => {
		if (ext) {
			ext.style.border = "";
			ext = null;
		}
	});
	$("*").on("mousemove",(e)=>{
		if(e!==ext){
			f(e);
		}
	});
},200)
});
